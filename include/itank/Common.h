#ifndef COMMON_H
#define COMMON_H

#include <cmath>

static const double tread = 0.15;   /* distance between tires a.k.a. tread */
static const double tire_rad = 0.0425;/* radius of tire */
static const double gear = 1.0;
static const int enc_val_max = 65535;
static const double enc_per_rot = 46; /* enc_val/tire_rot */
static const double meter_per_enc = 2. * M_PI * tire_rad / enc_per_rot;/* encoder val to meter */

#endif //COMMON_H
