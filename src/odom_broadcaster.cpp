#include <iostream>
#include <ros/console.h>

#include <itank/Common.h>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/UInt16MultiArray.h>
#include <nav_msgs/Odometry.h>

//parameter
/* move to ../include/itank/Common.h */
/* 
 * const double tread = 0.15; // distance between tires a.k.a. tread
 * const double tire_rad = 0.03; // radius of tire
 * const double gear = 1.0;
 * const int enc_val_max = 1024;
 * const double enc_per_rot = 20; // enc_val/tire_rot
 * const double meter_per_enc = 2. * M_PI * tire_rad / enc_per_rot; //encoder val to meter
 */

ros::Publisher odom_pub;
ros::Subscriber enc_sub;


unsigned int prev_enc_right = 0, prev_enc_left = 0;
double x = 0.0, y = 0.0, th = 0.0;
double v = 0.0, vth = 0.0;

ros::Time curr_time, prev_time;

void enc_cb(const std_msgs::UInt16MultiArray &msg){
    static tf::TransformBroadcaster odom_broadcaster;
    unsigned int enc_right = msg.data[0];
    unsigned int enc_left = msg.data[1];

    curr_time = ros::Time::now();
    double dt = (curr_time - prev_time).toSec();
    long int d_enc_right = (long int)enc_right - (long int)prev_enc_right;
    long int d_enc_left = (long int)enc_left - (long int)prev_enc_left;
    if (d_enc_right > 30000) d_enc_right -= enc_val_max;
    if (d_enc_left > 30000) d_enc_left -= enc_val_max;
    if (d_enc_right < -30000) d_enc_right += enc_val_max;
    if (d_enc_left < -30000) d_enc_left += enc_val_max;

    double omega_r = d_enc_right * 2.0 * M_PI / (enc_per_rot * gear * dt);
    double omega_l = d_enc_left * 2.0 * M_PI / (enc_per_rot * gear * dt);
    v = tire_rad / 2. * (omega_r + omega_l);
    vth = tire_rad / tread * (omega_r - omega_l);
    th += vth * dt;
    double vx = v * cos(th);
    double vy = v * sin(th);
    x += vx * dt;
    y += vy * dt;

    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = curr_time;
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "base_link";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;

    odom_broadcaster.sendTransform(odom_trans);

    nav_msgs::Odometry odom;
    odom.header.stamp = curr_time;
    odom.header.frame_id = "odom";

    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.position.z = 0.0;
    odom.pose.pose.orientation = odom_quat;

    odom.child_frame_id = "base_link";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    odom_pub.publish(odom);

    prev_time = curr_time;
    prev_enc_right = enc_right;
    prev_enc_left = enc_left;
}

int main(int argc, char const *argv[])
{
    ros::init(argc, (char**)argv, "iTank_odometry_publisher");

    ros::NodeHandle nh;
    ros::Rate r(20);
    odom_pub = nh.advertise<nav_msgs::Odometry>("odom", 50);
    enc_sub = nh.subscribe("arduinoSerialEncoder", 1, enc_cb);

    prev_time = ros::Time::now();
    curr_time = ros::Time::now();

	ROS_DEBUG("odom test");

    while(nh.ok()){
        ros::spinOnce();
        r.sleep();
    }

    return 0;
}
