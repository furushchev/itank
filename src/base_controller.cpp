#include <ros/ros.h>
#include <std_msgs/Int16MultiArray.h>
#include <geometry_msgs/Twist.h>
#include <cmath>
#include <itank/Common.h>

ros::Publisher throttle_pub;
ros::Subscriber base_controller_sub;
ros::Time curr_time, prev_time;
//int rate = 10;
int rate = 20;

void cmd_vel_Callback(const geometry_msgs::Twist::ConstPtr& cmd_vel);

int main(int argc, char** argv){
  ros::init(argc, argv, "itank_base_controller");
  ros::NodeHandle n;
  ros::Rate r(rate);
  throttle_pub = n.advertise<std_msgs::Int16MultiArray>("arduinoMotorVel", 10);
  base_controller_sub = base_controller_sub = n.subscribe("cmd_vel", 1, cmd_vel_Callback);
    
  prev_time = ros::Time::now();
  curr_time = ros::Time::now();

  while(n.ok()){
    ros::spinOnce();
    r.sleep();
  }
}

int16_t y(double a, double b, double x){
  return (int16_t)(a * x + b);
}

int16_t vel_to_throttle(double vel){
  int16_t throttle = 0;
  if (-0.67667 <= vel && vel < -0.58) throttle = y(206.89, -60, vel);
  else if (-0.58 <= vel && vel < -0.53333) throttle = y(428.57, 68.5714, vel);
  else if (-0.53333 <= vel && vel < -0.45) throttle = y(240, -32, vel);
  else if (-0.45 <= vel && vel < -0.37) throttle = y(250, -27.5, vel);
  else if (0.37 <= vel && vel < 0.45) throttle = y(200,50,vel);
  else if (0.45 <= vel && vel < 0.52333) throttle = y(272.7272727, 17.27272727, vel);
  else if (0.52333 <= vel && vel < 0.57333) throttle = y(400, -49.33333, vel);
  else if (0.57333 <= vel && vel < 0.67667) throttle = y(193.5483871, 69.03225806, vel);
  else throttle = 0;

  return throttle;
}

int16_t vel_to_throttle_right(double vel, double dt){
    double enc = vel * dt / meter_per_enc * 10;
    int16_t throttle;
    if(enc > 0) throttle = (int16_t)(0.0004 * pow(enc, 2) + 0.0462 * enc + 90);
    else if(enc < 0)        throttle = (int16_t)((-0.0004 * pow(enc, 2) - 0.0462 * enc - 90) * 1.2 -8);
    else return 0;
//    ROS_INFO("vel_r = %lf, dt = %lf, right = %d", vel, dt, throttle);
    return throttle;
}

int16_t vel_to_throttle_left(double vel, double dt){
    double enc = vel * dt / meter_per_enc * 1.2 * 10;
    int16_t throttle;
    if(enc > 0) throttle = (int16_t)(0.0004 * pow(enc, 2) + 0.0522 * enc + 90);
    else if (enc < 0) throttle = (int16_t)((-0.0004 * pow(enc, 2) - 0.0522 * enc - 90) * 1.2 - 4);
    else return 0;
//    ROS_INFO("vel_l = %lf, dt = %lf, left = %d", vel, dt, throttle);
    return throttle;
}

void cmd_vel_Callback(const geometry_msgs::Twist::ConstPtr& cmd_vel){
  curr_time = ros::Time::now();
//  ROS_INFO("curr_time = %lf, prev_time = %lf", curr_time.toSec(), prev_time.toSec());
  double dt = (curr_time - prev_time).toSec(); 
  if (dt > 1. / rate) dt = 1. / rate;
  prev_time = curr_time;
  double v = cmd_vel->linear.x;
  double omega = cmd_vel->angular.z;

  double v_right = v + tread * omega / 2.0;
  double v_left  = v - tread * omega / 2.0;

  if (fabs(v_right) < 0.37 || fabs(v_left) < 0.37) {
    if (0.5 <= omega && omega < 4.933333) omega = 4.93334;
    else if (-4.933333 < omega && omega < -0.5) omega = -4.93334;
    else if (-0.30 <= omega && omega < 0.30) omega = 0.0;
    v_right = v + tread * omega / 2.0;
    v_left  = v - tread * omega / 2.0;
  }

  std_msgs::Int16MultiArray throttle_array;
  std::vector<int16_t> send_data(2);
  
  /* the range of throttle is -200 ~ 200 */

  //  send_data[1] = vel_to_throttle_left(v_right, dt);
  //  send_data[0] = vel_to_throttle_right(v_left, dt);
  send_data[0] = vel_to_throttle(v_left);
  send_data[1] = vel_to_throttle(v_right);

  throttle_array.data = send_data;
  throttle_pub.publish(throttle_array);
//  ROS_INFO("Send Left Throttle:%d Right Throttle:%d", send_data[0], send_data[1]);
}
