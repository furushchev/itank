#include <ros/ros.h>
#include <std_msgs/Int16MultiArray.h>
#include <cmath>
#include <itank/Common.h>

ros::Publisher throttle_pub;
ros::Subscriber encoder_sub;

using namespace std;

// void encCb(const std_msgs::Int16MultiArray & msg){
//     int enc_right = msg.data[0];
//     int enc_left = msg.data[1];
//     double meter_right = enc_right * meter_per_enc;
//     double meter_left = enc_left * meter_per_enc;
//     ROS_INFO("enc_r: %d - %lfm, enc_l: %d, - %lfm", enc_right, meter_right, enc_left, meter_left);
// }

int main(int argc, char const *argv[])
{
    ros::init(argc, (char**)argv, "odom_calib");
    ros::NodeHandle nh;
    ros::Rate r(1.0);
    throttle_pub = nh.advertise<std_msgs::Int16MultiArray>("arduinoMotorVel", 10);
//    encoder_sub = nh.subscribe("arduinoSerialEncoder", 1, encCb);
    std_msgs::Int16MultiArray send_data;
    vector<int16_t> array(2);
    int throttle1 = 0, throttle2 = 0;
    while(nh.ok()){
        //ros::spin();
        //int throttle ;
        //throttle++;
        ROS_INFO("throttle1?: ");
        cin >> throttle1;
        ROS_INFO("throttle2?: ");
        cin >> throttle2;
        array[0] = throttle1;
        array[1] = throttle2;
        send_data.data = array;
        throttle_pub.publish(send_data);
//        r.sleep();
        usleep(3e6);
        ROS_INFO("send 0");
        array[0] = 0;
        array[1] = 0;
        send_data.data = array;
        throttle_pub.publish(send_data);
        //sleep(10);
    }

    return 0;
}
