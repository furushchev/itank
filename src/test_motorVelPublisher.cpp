#include <ros/ros.h>
#include <std_msgs/Int16MultiArray.h>

int main(int argc, char const* argv[])
{
    ros::init(argc, (char**)argv, "test_motorVelPublisher");
    
    ros::NodeHandle nh;
    ros::Publisher motorVel_pub = nh.advertise<std_msgs::Int16MultiArray>("arduinoMotorVel", 50);
    
    
    std_msgs::Int16MultiArray data_array;
    std::vector<int16_t> send_data;
    send_data.push_back(0);
    send_data.push_back(0);

    while (nh.ok()) {
        if(++send_data[0] > 255) send_data[0] = 0;
        if(--send_data[1] < 0) send_data[1] = 255;
        data_array.data = send_data;
        motorVel_pub.publish(data_array);
        ros::spinOnce();
        usleep(50);
    }

    return 0;
}
