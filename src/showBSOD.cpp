#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <highgui.h>
#include <cv.h>


ros::Subscriber fire_sub;

void fire_cb(const std_msgs::Bool &msg){
    if (msg.data == false) return;
    IplImage *img = cvLoadImage("~/ros/fuerte/jsk-ros-pkg/itank/src/bsod.jpg", CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
    cvNamedWindow("Name", CV_WINDOW_NORMAL);
    cvSetWindowProperty("Name", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
    cvShowImage("Name", img);
}

int main(int argc, char const* argv[])
{
    ros::init(argc, (char**)argv, "showBSOD");
    ros::NodeHandle nh;

    fire_sub = nh.subscribe("arduinoFire",1, fire_cb );


    return 0;
}
