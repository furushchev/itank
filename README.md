#ROS iTank パッケージ

## arduino teleop key
arduino から odometry を受け取り， motorVel を送ってモータを動かす．  

----
##事前準備  
iTank に載せる PC と rviz などを動かすサーバ PC の関係を設定する．  
サーバ PC と iTank はお互いが直接見えるようにネットワーク内に配置されている必要がある．
####サーバ PC 側  
まずサーバの IP アドレスを確認する．
```
#!bash
server $ ifconfig  # inet addr: xxx.xxx.xxx.xxx←これを控えておく
```
すべてのプログラム実行前に
```
#!bash
server $ rossetip
```
を実行しておく．  
####iTank PC側
すべてのプログラム実行前に
```
#!bash
iTank $ rossetrobot xxx.xxx.xxx.xxx #←控えておいたIP
iTank $ rossetip
```
を実行する．  
予め
```
#!bash
iTank $ function rossetitank(){
iTank $     rossetrobot $1
iTnak $     rossetip
iTank $ }
```
と `~/.bashrc` に定義しておくと
```
#!bash
iTank $ rossetitank xxx.xxx.xxx.xxx
```
とするだけで済む．  

----

###やり方
####サーバ PC 側
まず最初に setup する．
```
#!bash
$ roslaunch itank itank_configuration.launch
```
次にrviz を立ち上げる．
```
#!bash
$ rosrun rviz rviz
```
####iTank PC側
まず iTank レポジトリを ROS の PATH が通ったところへ git clone する
```
#!bash
$ git clone https://furushchev@bitbucket.org/furushchev/itank.git
```
次に iTank のプログラムをビルドする．
```
#!bash
$ roscd itank
$ rosmake
```
そして起動する．
```
#!bash
bash1 $ roscore
bash2 $ rosrun rosserial_python serial_node.py /path/to/arduino
bash3 $ rosrun itank base_controller
bash4 $ rosrun teleop_twist_keyboard teleop_twist_keyboard teleop_twist_keyboard.py
```